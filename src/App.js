import './App.css';
import Form from './components/Form';

function App() {
  return (
    <div className="App bg-red-200">
      <Form/>
    </div>
  );
}

export default App;
